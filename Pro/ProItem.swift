//
//  ProItem.swift
//  Pro
//
//  Created by Hen Shabat on 10/04/2019.
//  Copyright © 2019 Hen Shabat. All rights reserved.
//
import SwiftyJSON

class ProItem {
    
    var title: String
    var subTitle: String?
    var metricUnit: String?
    var minPrice: Int
    var maxPrice: Int
    var childArr: [ProItem]
    
    init(json: JSON) {
        self.title = json["title"].stringValue
        self.subTitle = json["subTitle"].string
        self.metricUnit = json["metricUnit"].string
        self.minPrice = json["minPrice"].intValue
        self.maxPrice = json["maxPrice"].intValue
        self.childArr = json["childArr"].arrayValue.compactMap { ProItem(json: $0) }
    }
    
}
