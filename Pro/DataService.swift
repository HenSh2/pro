//
//  DataService.swift
//  Pro
//
//  Created by Hen Shabat on 10/04/2019.
//  Copyright © 2019 Hen Shabat. All rights reserved.
//

import UIKit
import SwiftyJSON

class DataService {
    
    static let CELL_HEIGHT: CGFloat = 92
    
    class func items() -> [ProCellVM] {
        guard let path = Bundle.main.path(forResource: "prices", ofType: "json") else { return [ProCellVM]() }
        let url = URL(fileURLWithPath: path)
        guard let data = try? Data(contentsOf: url) else { return [ProCellVM]() }
        var items: [ProCellVM] = [ProCellVM]()
        let json: JSON = JSON(data)
        for item in json["data"]["priceItems"].arrayValue {
            items.append(ProCellVM(item: ProItem(json: item)))
        }
        return items
    }
    
}
