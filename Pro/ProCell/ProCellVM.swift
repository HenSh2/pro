//
//  ProCellVM.swift
//  Pro
//
//  Created by Hen Shabat on 10/04/2019.
//  Copyright © 2019 Hen Shabat. All rights reserved.
//
import UIKit

class ProCellVM {
    
    private var item: ProItem
    
    init(item: ProItem) {
        self.item = item
    }
    
    var title: String {
        return self.item.title
    }
    
    var subTitle: String? {
        return self.item.subTitle
    }
    
    var priceString: String {
        return "\(self.item.minPrice) - \(self.item.maxPrice)"
    }
    
    var metricUnitString: String? {
        return self.item.metricUnit
    }
    
    var isExpandable: Bool {
        return self.item.childArr.count > 0
    }
    
    var items: [ProCellVM] {
        return self.item.childArr.compactMap { ProCellVM(item: $0) }
    }
    
    var innerTableViewHeigh: CGFloat {
        return CGFloat(self.items.count) * DataService.CELL_HEIGHT
    }
    
}
