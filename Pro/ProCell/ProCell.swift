//
//  ProCell.swift
//  Pro
//
//  Created by Hen Shabat on 10/04/2019.
//  Copyright © 2019 Hen Shabat. All rights reserved.
//

import UIKit

class ProCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var metricUnitLabel: UILabel!
    
    @IBOutlet weak var titleLabelTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var dropdownImageView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightAnchor: NSLayoutConstraint!
    
    private let proCellId: String = "ProCell"
    
    var viewModel: ProCellVM!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup() {
        self.selectionStyle = .none
        self.titleLabel.text = self.viewModel.title
        self.subTitleLabel.text = self.viewModel.subTitle
        self.priceLabel.text = self.viewModel.priceString
        self.metricUnitLabel.text = self.viewModel.metricUnitString
        self.titleLabelTopAnchor.constant = self.viewModel.isExpandable ? 30 : 10
        self.priceLabel.isHidden = self.viewModel.isExpandable
        self.dropdownImageView.isHidden = !self.viewModel.isExpandable
        if self.viewModel.isExpandable {
            self.setupTableView()
        }
    }
    
    func rotateArrow(isOpen: Bool) {
        let angle: CGFloat = isOpen ? CGFloat.pi * 1.5 : CGFloat.pi * 2
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.dropdownImageView.transform = CGAffineTransform(rotationAngle: angle)
        }
    }
    
    private func setupTableView() {
        let nib: UINib = UINib(nibName: self.proCellId, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: self.proCellId)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableViewHeightAnchor.constant = self.viewModel.innerTableViewHeigh
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: self.proCellId, for: indexPath) as! ProCell
        cell.viewModel = self.viewModel.items[indexPath.row]
        cell.setup()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DataService.CELL_HEIGHT
    }
    
}
