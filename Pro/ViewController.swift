//
//  ViewController.swift
//  Pro
//
//  Created by Hen Shabat on 10/04/2019.
//  Copyright © 2019 Hen Shabat. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let proCellId: String = "ProCell"
    
    private var items: [ProCellVM] = [ProCellVM]()
    
    private var selectedIndexPath: IndexPath? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupController()
        self.setupTableView()
    }
    
    private func setupController() {
        self.items = DataService.items()
    }
    
    private func setupTableView() {
        let nib: UINib = UINib(nibName: self.proCellId, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: self.proCellId)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: self.proCellId, for: indexPath) as! ProCell
        cell.viewModel = self.items[indexPath.row]
        cell.setup()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !self.items[indexPath.row].isExpandable {
            return DataService.CELL_HEIGHT
        }
        return indexPath == self.selectedIndexPath ? self.items[indexPath.row].innerTableViewHeigh + DataService.CELL_HEIGHT : DataService.CELL_HEIGHT - 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.items[indexPath.row].isExpandable else { return }
        if self.selectedIndexPath == indexPath {
            if let cell = self.tableView.cellForRow(at: indexPath) as? ProCell {
                cell.rotateArrow(isOpen: false)
            }
            self.selectedIndexPath = nil
        } else {
            if let cell = self.tableView.cellForRow(at: indexPath) as? ProCell {
                cell.rotateArrow(isOpen: true)
            }
            if let ip = self.selectedIndexPath, let cell = self.tableView.cellForRow(at: ip) as? ProCell {
                cell.rotateArrow(isOpen: false)
            }
            self.selectedIndexPath = indexPath
        }
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        })
        tableView.beginUpdates()
        tableView.endUpdates()
        CATransaction.commit()
    }

}
